# NEO4J

Deze repository bevat de bestanden voor de NEO4J workshop van 19 december 2018

# Laden Archisurance dataset

Plaats de bestanden uit de directroy archisurance in de import directory van NEO4J

Start met een lege database

```
match(n) 
detach delete n
```

Nodes toevoegen

```
LOAD CSV WITH HEADERS FROM 'file:///elements.csv' AS line
CREATE (:elements {class:line.Type, name:line.Name, documentation:line.Documentation,
    id:line.ID })
```

Edges toevoegen

```
LOAD CSV WITH HEADERS FROM 'file:///relations.csv' AS line
MERGE (n {id:line.Source})
merge (m {id:line.Target})
with n,m,line
call apoc.create.relationship(n,line.Type,{id:line.ID, class:line.Type, documentation:line.Documentation,
    name:line.Type},m) YIELD rel
return rel
```

Tonen model

```
MATCH (n) RETURN *
```

# Uitbreiden model
## Labels voor verschillende elementen 
Het type element is met inlezen als property class opgeslagen. 
Hiermee kunnen ook Labels toegevoegd worden

```
MATCH (n:elements)
CALL apoc.create.addLabels(n, [ n.class ]) YIELD node
RETURN node
```

## Labels toevoegen voor layers
Toevoegen Strategy Layer

```
MATCH (n:elements)
WHERE n:Resource OR n:n:Capability OR n:CourseOfAction 
CALL apoc.create.addLabels(n, [ "StrategyLayer" ]) YIELD node
RETURN node
```

Business Layer toevoegen
```
MATCH (n:elements)
WHERE n:BusinessActor OR n:BusinessRole OR n:BusinessCollaboration 
    OR n:BusinessInterface OR n:BusinessProcess OR n:BusinessFunction 
	OR n:BusinessInteraction OR n:BusinessEvent OR n:BusinessService 
	OR n:BusinessObject OR n:Contract OR n:Representation
	OR n:Product
CALL apoc.create.addLabels(n, [ "BusinessLayer" ]) YIELD node
RETURN node
```

Application Layer toevoegen

```
MATCH (n:elements)
WHERE n:ApplicationComponent OR n:n:ApplicationCollaboration OR n:ApplicationInterface 
    OR n:ApplicationFunction OR n:ApplicationInteraction OR n:ApplicationProcess 
	OR n:ApplicationEvent OR n:ApplicationService OR n:DataObject
CALL apoc.create.addLabels(n, [ "ApplicationLayer" ]) YIELD node
RETURN node
```

Technology Layer toevoegen

```
MATCH (n:elements)
WHERE n:Node OR n:Device OR n:SystemSoftware OR n:TechnologyCollaboration OR n:TechnologyInterface 
    OR n:Path OR n:CommunicationNetwork OR n:TechnologyFunction OR n:TechnologyProcess OR n:TechnologyInteraction
    OR n:TechnologyEvent OR n:TechnologyService OR n:Artifact OR n:Equipment OR n:Facility OR n:DistributionNetwork  
	OR n:Material
CALL apoc.create.addLabels(n, [ "TechnologyLayer" ])
YIELD node
RETURN node
```

Motivation Layer toevoegen

```
MATCH (n:elements)
WHERE n:Stakeholder OR n:Driver OR n:Assesment OR n:Goal OR n:Outcome
    OR n:Principle OR n:Requirement OR n:Constraint
	OR n:Meaning OR n:Value 
CALL apoc.create.addLabels(n, [ "Motivation" ])
YIELD node
RETURN node
```

Implementation Layer toevoegen

```
MATCH (n:elements)
WHERE n:WorkPackage OR n:Deliverable OR n:ImplementationEvent OR n:Plateau OR n:Gap
CALL apoc.create.addLabels(n, [ "Implementation" ])
YIELD node
RETURN node
```

## Zoeken in Neo4J

Zoek alle elementen met name "Firewall"

```
match(elements{name:'Firewall'}) return elements
```

Zoek de elementen met name "Firewall" en hun relaties

```
match(elements{name:'Firewall'})-[r]->(c) return *
```

Dieper langs de relaties

```
match(elements{name:'Firewall'})-[r*1..3]-(c) return *
```

Onderscheid in richting van de relaties

```
match(elements{name:'Firewall'})-[r*1..3]->(c) return *
```

```
match(elements{name:'Firewall'})<-[r*1..3]-(c) return *
```
